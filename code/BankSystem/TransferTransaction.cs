﻿namespace BankSystem;

public class TransferTransaction : Transaction
{
    public Account AccountTo {get; private set;}
    public TransferTransaction(int transactionCost, Account accountFrom, Account accountTo) : base(transactionCost, accountFrom)
    {
        AccountTo = accountTo;
    }

    public override TransactionInfo Execute()
    {
        if(AccountFrom.Withdraw(TransactionCost) && AccountTo.Deposit(TransactionCost))
        {
            Info = $"TRANSFER FROM {AccountFrom.Number} TO {AccountTo.Number} {TransactionCost} - success";
            return new TransactionInfo(Info, TransactionResult.Success);
        }
        else
        {
            Info = $"TRANSFER FROM {AccountFrom.Number} TO {AccountTo.Number} {TransactionCost} - failure";
            return new TransactionInfo(Info, TransactionResult.Failure);
        }
    }
}