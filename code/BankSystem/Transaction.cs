﻿namespace BankSystem;

public abstract class Transaction
{
    protected int TransactionCost { get; set; }
    protected string Info { get; set; } = "not executed";
    protected Account AccountFrom { get; set; }
    public abstract TransactionInfo Execute();
    public Transaction(int transactionCost, Account accountFrom)
    {
        TransactionCost = transactionCost;
        AccountFrom = accountFrom;
    }

    // a field for demonstrating the operation of the system
    public string TranInfo 
    {
        get => Info; 
    }
}