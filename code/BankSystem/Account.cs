﻿namespace BankSystem;

public class Account
{
    public Card? Card { get; private set; }
    public string Number { get; private set; }
    public int Balance { get; private set; }
    public Account(Card card, string number, int balance)
    {
        Card = card;
        Number = number;
        Balance = balance;
    }

    public bool Deposit(int amount) {Balance += amount; return true;}
    public bool Withdraw(int amount)
    {
        if(Balance >= amount)
        {
            Balance -= amount;
            return true;
        }
        return false;
    }
}