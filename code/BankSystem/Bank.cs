﻿namespace BankSystem;

public class Bank
{
    private List<Account> _accounts;
    private List<Transaction> _transactions;

    // a field for demonstrating the operation of the system
    public List<Transaction> Transactions
    {
        get => _transactions;
    }

    public Bank()
    {
        _accounts = new();
        _transactions = new();
    }
    
    public Bank(List<Account> accounts, List<Transaction> transactions)
    {
        _accounts = accounts;
        _transactions = transactions;
    }

    public Account? GetAccountByCard(Card card) => _accounts.FirstOrDefault(a => a.Card == card);
    public bool AccountExists(Account account) => _accounts.Exists(a => a == account);
    public void AddTransaction(Transaction transaction) => _transactions.Add(transaction);
}