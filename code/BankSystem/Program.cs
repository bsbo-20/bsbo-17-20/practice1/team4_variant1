﻿namespace BankSystem;

class Program
{
    static void Main()
    {
        #region test data defenition
        Card card = new Card(1234);
        Account clientAccount = new Account(card, "88", 6000);
        Account organisationAccount = new Account(null, "12", 6500000);

        Bank bank = new Bank(
            new List<Account>(){clientAccount, organisationAccount},
            new List<Transaction>()
        );

        Receipt receipt = new Receipt(organisationAccount, "Utility bills", 4000);
        #endregion

        #region PERVIOUS INFO OUTPUT
        System.Console.WriteLine("ACCOUNTS INFO\n" + new string('-', 20));
        System.Console.WriteLine($"Client:\n Card PIN:1234\n Account number: {clientAccount.Number}\n Balance: {clientAccount.Balance}");
        System.Console.WriteLine($"Organisation:\n Account number: {organisationAccount.Number}\n Balance: {organisationAccount.Balance}");
        System.Console.WriteLine(new string('-', 20));
        System.Console.WriteLine("RECEIPT INFO");
        System.Console.WriteLine($" Account number: {receipt.Account}\n Info: {receipt.ReceiptInfo}\n Payment amount: {receipt.PaymentAmount}");
        System.Console.WriteLine(new string('-', 20));
        #endregion

        var ATM = new ATM(bank: bank, moneyAmount:100000);

        System.Console.WriteLine("*Client inserts card into atm");
        ATM.PutCard(card);

        System.Console.WriteLine("*Client checks balance");
        System.Console.WriteLine(ATM.CheckBalance());

        System.Console.WriteLine("*Client pays receipt");
        ATM.PayReceipt(receipt);

        System.Console.WriteLine(new string('-', 20));
        System.Console.WriteLine("BANK TRANSACTIONS LIST:");

        foreach(var transaction in bank.Transactions)
        {
            System.Console.WriteLine(transaction.TranInfo);
        }

    }
}
