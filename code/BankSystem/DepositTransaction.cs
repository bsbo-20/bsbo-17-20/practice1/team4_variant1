﻿namespace BankSystem;
public class DepositTransaction : Transaction
{
    public DepositTransaction(int transactionCost,  Account accountFrom) : base(transactionCost, accountFrom)
    {
    }

    public override TransactionInfo Execute()
    {
        if(AccountFrom.Deposit(TransactionCost))
        {
            Info = $"DEPOSIT TO {AccountFrom.Number} {TransactionCost} - success";
            return new TransactionInfo(Info, TransactionResult.Success);
        }
        else
        {
            Info = $"DEPOSIT TO {AccountFrom.Number} {TransactionCost} - failure";
            return new TransactionInfo(Info, TransactionResult.Failure);
        }
    }
}