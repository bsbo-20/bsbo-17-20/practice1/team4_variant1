﻿namespace BankSystem;

public class ATM
{
    private Bank _bank;
    private TransactionManager _transactionManager;
    private bool _hasDye;
    private bool _hasPaper;
    private int _moneyAmount;
    private Account? _clientAccount;

    public ATM(Bank bank, bool hasDye = true, bool hasPaper = true, int moneyAmount = 0)
    {
        _bank = bank;
        _hasDye = hasDye;
        _hasPaper = hasPaper;
        _moneyAmount = moneyAmount;
        _clientAccount = null;
        _transactionManager = new TransactionManager(_bank);
    }

    private void GetMoney(int amount) => _moneyAmount += amount;
    private void GiveMoney(int amount)
    {
        _moneyAmount -= amount;
        System.Console.WriteLine("*ATM dispenses money");
    }
    private void PrintCheck(Check check)
    {
        if (_hasDye && _hasPaper)
        {
            System.Console.WriteLine($"*ATM dispenses this check: {check.CheckInfo}");
        }
        else
        {
            System.Console.WriteLine($"*ATM is out of paper or ink. Check info: {check.CheckInfo}");
        }
    }
    private void ReturnCard() 
    {
        System.Console.WriteLine("*ATM returns card");
        EndService();
    }
    private void EndService() => this._clientAccount = null;
    public void PutCard(Card card)
    {
        if (!card.IsValid)
        {
            System.Console.WriteLine("CARD IS NOT VALID.");
            ReturnCard();
            return;
        }
        System.Console.WriteLine("INPUT PIN: ");
        int trysCount = 0;
        
        int pin = Convert.ToInt32(Console.ReadLine());
        trysCount++;

        while(!card.CheckPIN(pin))
        {
            if (trysCount == 3)
            {
                System.Console.WriteLine("TOO MANY INPUT ATTEMPTS. CARD BLOCKED. END SERVICE");
                card.Block();
                ReturnCard();
                return;
            }
            System.Console.WriteLine("PIN ISN'T CORRECT TRY AGAIN:");
            pin = Convert.ToInt32(Console.ReadLine());
            trysCount++;
        }

        _clientAccount = _bank.GetAccountByCard(card);
    }
    public void WithdrawMoney(int amount)
    {
        #region condition checkout
        if(_clientAccount is null)
        {
            return;
        }
        if (_moneyAmount < amount)
        {
            System.Console.WriteLine("*ATM doesn't have enough money");
            return;
        }
        #endregion
        var transaction = new WithdrawTransaction(amount, _clientAccount);
        var transactionResult = _transactionManager.ExecuteTransaction(transaction);

        if(transactionResult.Result == TransactionResult.Success)
        {
            PrintCheck(transactionResult.MakeCheck());
        }
        else
        {
            System.Console.WriteLine($"TRANSACTION ERROR. INFO: {transactionResult.Info}");
        }
        ReturnCard();
    }
    public void DepositMoney(int amount)
    {
        #region condition checkout
        if(_clientAccount is null)
        {
            return;
        }
        #endregion
        var transaction = new DepositTransaction(amount, _clientAccount);
        var transactionResult = _transactionManager.ExecuteTransaction(transaction);

        if (transactionResult.Result == TransactionResult.Success)
        {
            PrintCheck(transactionResult.MakeCheck());
        }
        else
        {
            System.Console.WriteLine($"TRANSACTION ERROR. INFO: {transactionResult.Info}");
        }
        ReturnCard();
    }
    public int CheckBalance()
    {
        if (_clientAccount == null)
        {
            return 0;
        }
        return _clientAccount.Balance;
    }
    public void PayReceipt(Receipt receipt)
    {
        #region condition checkout
        if (_clientAccount is null)
        {
            return;
        }
        if(!_bank.AccountExists(receipt.Account))
        {
            System.Console.WriteLine("*Account from receipt not exists");
            ReturnCard();
            return;
        }
        if(_clientAccount.Balance < receipt.PaymentAmount)
        {
            System.Console.WriteLine("YOU HAVE NOT ENOUGH MONEY");
            ReturnCard();
            return;
        }
        #endregion
        
        var transaction = new TransferTransaction(
            receipt.PaymentAmount, _clientAccount, receipt.Account
        );
        var transactionResult = _transactionManager.ExecuteTransaction(transaction);

        if(transactionResult.Result == TransactionResult.Success)
        {
            PrintCheck(transactionResult.MakeCheck());
        }
        else
        {
            System.Console.WriteLine($"TRANSACTION ERROR. INFO: {transactionResult.Info}");
        }

        ReturnCard();
    }
}