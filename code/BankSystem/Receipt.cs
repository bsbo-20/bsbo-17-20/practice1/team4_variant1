﻿namespace BankSystem;

public class Receipt
{
    public readonly Account Account;
    public readonly string ReceiptInfo;
    public readonly int PaymentAmount;

    public Receipt(Account account, string receiptInfo, int paymentAmount)
    {
        Account = account;
        ReceiptInfo = receiptInfo;
        PaymentAmount = paymentAmount;
    }
}