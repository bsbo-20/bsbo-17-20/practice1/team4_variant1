﻿namespace BankSystem;

public class TransactionManager
{
    private Bank _bank;
    public TransactionManager(Bank bank)
    {
        _bank = bank;
    }
    public TransactionInfo ExecuteTransaction(Transaction transaction)
    {
        var result = transaction.Execute();
        _bank.AddTransaction(transaction);
        return result;
    }
}