﻿namespace BankSystem;

public class WithdrawTransaction : Transaction
{
    public WithdrawTransaction(int transactionCost, Account accountFrom) : base(transactionCost, accountFrom)
    {
    }

    public override TransactionInfo Execute()
    {
        if(AccountFrom.Withdraw(TransactionCost))
        {
            Info = $"WITHDRAW FROM {AccountFrom.Number} {TransactionCost} - success";
            return new TransactionInfo(Info, TransactionResult.Success);
        }
        else
        {
            Info = $"WITHDRAW FROM {AccountFrom.Number} {TransactionCost} - failure";
            return new TransactionInfo(Info, TransactionResult.Success);
        }
    }
}