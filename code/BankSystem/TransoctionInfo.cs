﻿namespace BankSystem;

public class TransactionInfo
{
    public readonly string Info;
    public readonly TransactionResult Result;

    public TransactionInfo(string transactionInfo, TransactionResult result)
    {
        Info = transactionInfo;
        Result = result;
    }

    public Check MakeCheck() => new Check(Info);

}