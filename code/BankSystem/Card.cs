﻿namespace BankSystem;

public class Card
{
    private int _pin;
    public bool IsValid { get; private set; }

    public Card(int pin)
    {
        _pin = pin;
        IsValid = true;
    }

    public bool CheckPIN(int pin) => pin == _pin;
    public void Block() => IsValid = false;
}