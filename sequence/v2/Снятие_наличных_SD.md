```mermaid
sequenceDiagram
actor Client
participant ATM
participant Card
participant TransactionManager
participant Transaction
participant Account
participant Bank
participant TransactionInfo

Client ->> ATM: WithdrawMoney(int amount)
ATM ->> ATM: Проверяет достаточно ли средств локально
ATM ->> Account: Проверяет достаточно ли средств на счёте
Account -->> ATM: result

break Когда недостаточно средств у клиента или банкомата
    ATM ->> Client: Выводит сообщение об ошибке
    Client ->> ATM: Получил сообщение об ошибке
    ATM ->> ATM: ReturnCard()
    ATM ->> ATM: EndService()
    ATM ->> Client: Вернул карту
    Client ->> ATM: Забрал карту
    ATM ->> Client: Завершил обслуживание   
end

ATM ->> TransactionManager: WithdrawTransaction(Account account, int amount)
TransactionManager ->> Transaction: Execute()
Transaction ->> Account: WithdrawMoney(int amount)
Account -->> Transaction: bool
Transaction -->> TransactionManager: TransactionInfo
TransactionManager ->> Bank: AddTransaction(Transaction transaction)
Bank ->> TransactionManager: Транзакция добавлена
TransactionManager -->> ATM:TransactionInfo   
alt Транзакция завершена успешно
    ATM ->> TransactionInfo: MakeCheck()
    TransactionInfo -->> ATM: Check 
    alt В принтере есть бумага и краска
        ATM ->> ATM: PrintCheck(Check check)
        ATM ->> ATM: ReturnCard()
        ATM ->> ATM: EndService()
        ATM ->> Client: Вернул карту
        Client ->> ATM: Забрал карту
        ATM ->> Client: Завершил обслуживание 
    else В принтере закончилась бумага или краски
        ATM ->> Client: Отображает чек
        Client ->> ATM: Видит чек на экране
        ATM ->> ATM: ReturnCard()
        ATM ->> ATM: EndService()
        ATM ->> Client: Вернул карту
        Client ->> ATM: Забрал карту
        ATM ->> Client: Завершил обслуживание 
    end
else Транзакция отклонена
    ATM ->> Client: Выводит сообщение об ошибке
    Client ->> ATM: Получил сообщение об ошибке
    ATM ->> ATM: ReturnCard()
    ATM ->> ATM: EndService()
    ATM ->> Client: Вернул карту
    Client ->> ATM: Забрал карту
    ATM ->> Client: Завершил обслуживание 
end
```