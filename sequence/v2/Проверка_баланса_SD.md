```mermaid
sequenceDiagram
actor Client
participant ATM
participant Account

Client ->> ATM: CheckBalance()
ATM ->> Account: Получает баланс
Account -->> ATM: Balance 
ATM ->> Client: Показывает счет
Client ->> ATM: Проверил счет
ATM ->> ATM: ReturnCard()
ATM ->> ATM: EndService()
ATM ->> Client: Вернул карту
Client ->> ATM: Забрал карту
ATM ->> Client: Завершил обслуживание 
```