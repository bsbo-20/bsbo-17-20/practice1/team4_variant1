```mermaid
sequenceDiagram
actor Client
participant ATM
participant Card
participant TransactionManager
participant Transaction
participant Account
participant Bank
participant TransactionInfo

Client ->> ATM: PayReceipt(Receipt receipt)
ATM ->> Bank: Проверяет наличие счета квитанции
Bank -->> ATM: bool
ATM ->> Account: Проверяет достаточно ли средств
Account -->> ATM: result 
break Когда не хватает средств для оплаты, либо счет квитанции не найден
    ATM ->> Client: Выводит сообщение об ошибке
    Client ->> ATM: Получил сообщение об ошибке
    ATM ->> ATM: ReturnCard()
    ATM ->> ATM: EndService()
    ATM ->> Client: Вернул карту
    Client ->> ATM: Забрал карту
    ATM ->> Client: Завершил обслуживание  
end

ATM ->> TransactionManager: TransferTransaction
TransactionManager ->> Transaction: Execute()
Transaction ->> Account: TransferTransaction.AccountFrom.WithdrawMoney(int amount)
Account -->> Transaction: bool
Transaction ->> Account: TransferTransaction.AccountTo.DepositMoney(int amount)
Account -->> Transaction: bool
Transaction -->> TransactionManager: TransactionInfo
TransactionManager ->> Bank: AddTransaction(Transaction transaction)
Bank ->> TransactionManager: Транзакция добавлена
TransactionManager -->> ATM: TransactionInfo   

alt Транзакция завершена успешно
    ATM ->> TransactionInfo: MakeCheck()
    TransactionInfo -->> ATM: Check 
    alt В принтере есть бумага и краска
        ATM ->> ATM: PrintCheck(Check check)
        ATM ->> ATM: ReturnCard()
        ATM ->> ATM: EndService()
        ATM ->> Client: Вернул карту
        Client ->> ATM: Забрал карту
        ATM ->> Client: Завершил обслуживание 
    else В принтере закончилась бумага или краски
        ATM ->> Client: Отображает чек
        Client ->> ATM: Видит чек на экране
        ATM ->> ATM: ReturnCard()
        ATM ->> ATM: EndService()
        ATM ->> Client: Вернул карту
        Client ->> ATM: Забрал карту
        ATM ->> Client: Завершил обслуживание 
    end
else Транзакция отклонена
    ATM ->> Client: Выводит сообщение об ошибке
    Client ->> ATM: Получил сообщение об ошибке
    ATM ->> ATM: ReturnCard()
    ATM ->> ATM: EndService()
    ATM ->> Client: Вернул карту
    Client ->> ATM: Забрал карту
    ATM ->> Client: Завершил обслуживание 
end
```