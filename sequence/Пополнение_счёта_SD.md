```mermaid
sequenceDiagram
actor Client
participant ATM
participant Card
participant TransactionManager
participant Transaction
participant Account
participant Bank
participant TransactionInfo

Client ->> ATM: PutCard(Card card)


loop
    ATM ->> Client: Просит ввести PIN
    Client ->> ATM: Вводит PIN
    ATM ->> Card: CheckPIN(int pin)
    Card -->> ATM: bool


    break Когда клиент ввел PIN неверно 3 раза
        ATM ->> Card: Block()
        Card ->> ATM: Карта заблокирована
        ATM ->> Client: Сообщает что карта заблокирована
        Client ->> ATM: Получил сообщение о блокировке
        ATM ->> ATM: ReturnCard()
        ATM ->> ATM: EndService()
        ATM ->> Client: Возвращает карту клиенту
        Client ->> ATM: Забрал карту
        ATM ->> Client: Завершил обслуживание     
    end 

    break Когда карта заблокирована
        ATM ->> Client: Сообщает что карта заблокирована
        Client ->> ATM: Получил сообщение о блокировке
        ATM ->> ATM: ReturnCard()
        ATM ->> ATM: EndService()
        ATM ->> Client: Возвращает карту клиенту
        Client ->> ATM: Забрал карту
        ATM ->> Client: Завершил обслуживание    
    end   
end

ATM ->> Bank: GetAccountByCard(Card card)
Bank -->> ATM: Account

ATM ->> Client: Просит внести деньги
Client ->> ATM: GetMoney(int amount)
ATM ->> TransactionManager: DepositTransaction(Account account, int amount)
TransactionManager ->> Transaction: Execute()
Transaction ->> Account: DepositMoney(int amount)
Account -->> Transaction: bool
Transaction -->> TransactionManager: TransactionInfo
TransactionManager ->> Bank: AddTransaction(Transaction transaction)
Bank ->> TransactionManager: Транзакция добавлена
TransactionManager -->> ATM:TransactionInfo   
alt Транзакция завершена успешно
    ATM ->> TransactionInfo: MakeCheck()
    TransactionInfo -->> ATM: Check 
    alt В принтере есть бумага и краска
        ATM ->> ATM: PrintCheck(Check check)
        ATM ->> ATM: ReturnCard()
        ATM ->> ATM: EndService()
        ATM ->> Client: Вернул карту
    else В принтере закончилась бумага или краски
        ATM ->> Client: Отображает чек
        Client ->> ATM: Видит чек на экране
        ATM ->> ATM: ReturnCard()
        ATM ->> ATM: EndService()
        ATM ->> Client: Вернул карту
        Client ->> ATM: Забрал карту
        ATM ->> Client: Завершил обслуживание 
    end
else Транзакция отклонена
    ATM ->> Client: Выводит сообщение об ошибке
    Client ->> ATM: Получил сообщение об ошибке
    ATM ->> ATM: GiveMoney(int amount)
    ATM ->> Client: Вернул деньги
    Client ->> ATM: Получил деньги
    ATM ->> ATM: ReturnCard()
    ATM ->> ATM: EndService()
    ATM ->> Client: Вернул карту
    Client ->> ATM: Забрал карту
    ATM ->> Client: Завершил обслуживание 
end
```