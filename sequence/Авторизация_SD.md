```mermaid
sequenceDiagram
actor Client
participant ATM
participant Card
participant Bank
participant Account

Client ->> ATM: PutCard(Card card)
loop
    ATM ->> Client: Просит ввести PIN
    Client ->> ATM: Вводит PIN
    ATM ->> Card: CheckPIN(int pin)
    Card -->> ATM: bool

    break Когда клиент ввел PIN неверно 3 раза
        ATM ->> Card: Block()
        Card ->> ATM: Карта заблокирована
        ATM ->> Client: Сообщает что карта заблокирована
        Client ->> ATM: Получил сообщение о блокировке
        ATM ->> ATM: ReturnCard()
        ATM ->> ATM: EndService()
        ATM ->> Client: Возвращает карту клиенту
        Client ->> ATM: Забрал карту
        ATM ->> Client: Завершил обслуживание     
    end 

    break Когда карта заблокирована
        ATM ->> Client: Сообщает что карта заблокирована
        Client ->> ATM: Получил сообщение о блокировке
        ATM ->> ATM: ReturnCard()
        ATM ->> ATM: EndService()
        ATM ->> Client: Возвращает карту клиенту
        Client ->> ATM: Забрал карту
        ATM ->> Client: Завершил обслуживание    
    end
end

ATM ->> Bank: GetAccountByCard(Card card)
Bank ->> Account:  new Account
Account -->> Bank: Account
Bank -->> ATM: Account
ATM ->> ATM: Локально сохраняет полученный аккаунт
ATM ->> Client: Обслуживает

```